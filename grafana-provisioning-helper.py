#!/usr/bin/env python

import os
import sys
import json
import logging
import requests

logging.basicConfig(
  level = logging.INFO,
  format = '{"time": "%(asctime)s", "level":"%(levelname)s", "message": "%(message)s"}',
  handlers = [
    logging.StreamHandler(sys.stdout)
  ]
)

try:
  BASE_URL = os.environ["BASE_URL"]
  logging.info("BASE_URL = " + BASE_URL)
except KeyError:
  logging.error("BASE_URL is undefined")
  sys.exit(1)

try:
  USERNAME = os.environ["USERNAME"]
  logging.info("USERNAME = " + USERNAME)
except KeyError:
  logging.error("USERNAME is undefined")
  sys.exit(1)

try:
  PASSWORD = os.environ["PASSWORD"]
  logging.info("PASSWORD len = " + str(len(PASSWORD)))
except KeyError:
  logging.error("PASSWORD is undefined")
  sys.exit(1)


s = requests.Session()
headers = {
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:70.0) Gecko/20100101 Firefox/70.0',
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'ru,en-US;q=0.7,en;q=0.3',
  'Content-Type': 'application/json;charset=utf-8',
  'Origin': BASE_URL + '',
  'DNT': '1',
  'Connection': 'keep-alive',
  'Referer': BASE_URL + '/login',
  'Pragma': 'no-cache',
  'Cache-Control': 'no-cache',
  'TE': 'Trailers'
}


r = s.post(
  BASE_URL + '/login',
  data = json.dumps({
    "user": USERNAME,
    "password": PASSWORD,
    "email": ""
  }),
  headers = headers
)

try:
  assert r.json() == {'message': 'Logged in'}
except AssertionError:
  logging.error(r.text)
  sys.exit(1)


plugins = [ plugins["id"] for plugins in s.get(BASE_URL + '/api/plugins/').json() ]

for plugin in plugins:
  rr = s.post(
    BASE_URL + '/api/plugins/' + plugin + '/settings',
    data = '{"enabled":true,"pinned":true,"jsonData":null}',
    headers = headers
  )
  if rr.ok:
    logging.info("plugin " + plugin + " " + str(rr.ok))


dsids = [ ds["id"] for ds in s.get(BASE_URL + '/api/datasources/').json() ]


for dsid in dsids:
  dsid = str(dsid)
  rrr = s.get(
    BASE_URL + '/api/datasources/' + dsid,
    headers = headers
  )
  if not rrr.ok:
    raise Exception(rrr.text)
  rrr = rrr.json()
  rrr["secureJsonFields"]["tlsCACert"] = True
  rrr["secureJsonFields"]["tlsClientCert"] = True
  rrr["secureJsonFields"]["tlsClientKey"] = True
  
  if rrr["type"] == "devopsprodidy-kubegraf-datasource":
    rrr["jsonData"]["tlsAuthWithCACert"] = True
    rrr["jsonData"]["tlsAuth"] = True
    rrr["jsonData"]["refresh_pods_rate"] = "15"
    rrr["jsonData"]["cluster_url"] = rrr["url"]


  if rrr["type"] == "grafana-kubernetes-datasource":
    rrr["jsonData"]["tlsAuthWithCACert"] = True
    rrr["jsonData"]["tlsAuth"] = True
    rrr["jsonData"]["tlsSkipVerify"] = False
  
  rrr = json.dumps(rrr)
  rrr = s.put(
    BASE_URL + '/api/datasources/' + dsid,
    headers = headers,
    data = rrr
  )
  logging.info("datasource " + dsid + " " + str(rrr.ok))


