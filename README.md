# grafana-provisioning-helper

Since grafana developers cannot implement auto-enable for installed applications and good IaaC, I implemented simple monkeypatch.

## quickstart

    wget -O artifacts.zip https://gitlab.com/egeneralov/grafana-provisioning-helper/-/jobs/artifacts/master/download?job=build-python
    export BASE_URL="https://grafana.example.com"
    export USERNAME=egeneralov
    export PASSWORD=egeneralov
    ./grafana-provisioning-helper

## What will happens

- all plugins will be enabled
- all datasources will be enabled
  - for [devopsprodigy-kubegraf-datasource](https://grafana.com/grafana/plugins/devopsprodigy-kubegraf-app)
    - enabled:
      - tlsAuthWithCACert
      - tlsAuth
    - rewrited:
      - cluster_url = datasource url
      - refresh_pods_rate = 15
    - disabled:
      - tlsSkipVerify
  - for [grafana-kubernetes-datasource](https://grafana.com/grafana/plugins/grafana-kubernetes-app) will be
    - enabled:
      - tlsAuthWithCACert
      - tlsAuth
    - disabled:
      - tlsSkipVerify

## helm usage

    # directory for our helm charts
    mkdir -p .helm
    # download grafana helm chart
    helm fetch stable/grafana
    tar xzvf grafana-*.tgz -C .helm/
    # download grafana-provisioning-helper helm chart
    wget -O artifacts.zip https://gitlab.com/egeneralov/grafana-provisioning-helper/-/jobs/artifacts/0.0.4/download?job=build-helm
    unzip artifacts.zip
    tar xzvf grafana-provisioning-helper-0.0.4.tgz -C .helm
    # cleanup
    rm artifacts.zip grafana-*.tgz
    # prepare and edit values
    cp .helm/grafana-provisioning-helper/values.yaml values-grafana-provisioning-helper-production.yaml
    cp .helm/grafana/values.yaml values-grafana-production.yaml
    ${EDITOR:-nano} values*.yaml
    # install grafana
    OPTS=monitoring-grafana .helm/grafana -f values-grafana-production.yaml --namespace monitoring --wait
    helm install --name ${OPTS} || helm upgrade ${OPTS}
    # install grafana-provisioning-helper
    OPTS=monitoring-grafana-provisioning-helper .helm/grafana-provisioning-helper -f values-grafana-provisioning-helper-production.yaml --namespace monitoring --wait
    helm install --name ${OPTS} || helm upgrade ${OPTS}

## Feature Request

Just open an issue or send an email to [eduard@generalov.net](mailto:eduard@generalov.net)

















