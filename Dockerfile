FROM python:3.7.4
WORKDIR /app/
ADD requirements.txt /app/
RUN pip install -r requirements.txt
ADD . .
RUN pyinstaller -F grafana-provisioning-helper.py --distpath ${PWD}

FROM debian:buster-slim
COPY --from=0 /app/grafana-provisioning-helper /
CMD /grafana-provisioning-helper

